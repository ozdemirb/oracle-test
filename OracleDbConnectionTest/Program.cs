﻿using System;
using System.Data.OracleClient;

namespace OracleDbConnectionTest
{
    class Program
    {
        static void Main(string[] args)
        {
            
            var connectionString = "Data Source = (DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = paymentpoc.cf1od9nt0c7n.us-east-2.rds.amazonaws.com)(PORT = 1521))(CONNECT_DATA = (SERVICE_NAME = ORCL))); User ID = payment; Password = payment12345";

            var dbCon = new OracleConnection(connectionString);

            try{
                dbCon.Open();    
                OracleCommand oraCommand = new OracleCommand("SELECT TO_CHAR(SYSDATE,'MM-DD-YYYY HH24:MI:SS') FROM DUAL",dbCon);
                OracleDataReader oraReader = null;
                oraReader = oraCommand.ExecuteReader();

                if (oraReader.HasRows)
                {
                    while (oraReader.Read())
                    {
                        Console.WriteLine(oraReader.GetString(0));
                    }
                }
                else
                {
                    Console.WriteLine("No Rows Found") ;
                }
            }
            catch(Exception ex){
                Console.WriteLine(ex.ToString());
            }


            //OracleCommand dbCommand = dbCon.CreateCommand();
            //string sql = "Select name,job FROM Employee";
            //dbCommand.CommandText = sql;

            //OracleDataReader reader = dbCommand.ExecuteReader();
            //while(reader.Read()){
            //    string employeeName = (string)reader["ename"];
            //    string job = (string)reader["job"];
            //    Console.WriteLine("Employee Name: {0} Job :{1}",employeeName,job);
            //}

            //reader.Close();
            //dbCommand.Dispose();
            //dbCon.Close();

            //Console.WriteLine("Hello World!");
        }
    }
}
